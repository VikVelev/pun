pragma solidity 0.4.24;

//pragma experimental ABIEncoderV2;

contract Better {
    event BetWon(address indexed user, uint indexed eventID, uint betIndex, uint winnings);
    
    struct Bet {
        uint amount;
        uint eventID;
        uint id;
        uint betType; //docs in 'betTypes' in Event struct
        uint[2] values;
        uint odd;
        address user;
        uint number;
    }
    
    
    struct Event {
        uint id;
        uint ts;
        string home;
        string away;
        string sport;
        uint[] betTypes; //0 = score, 1 = goals, 2 = result, 3 = penalty
        uint[] closedBetTypes;
        mapping(uint => bool) isBetTypeClosed;
        mapping(uint => uint) idToIndex;
        Bet[] availableBets;
        uint[] availableBetsIds;
        Bet[] placedBets;
        
        bool active;
    }
    
    uint[] private eventIDs;
    
    mapping(uint => Event) events;
    
    mapping(address => uint) withdrawals;
    
    address owner;
    
    modifier onlyOwner() {
        require(owner == msg.sender);
        _;
    }
    
    modifier eventExists(uint eventID) {
        //require(events[eventID].ts > 0);
        _;
    }
    
    modifier betExists(uint eventID, uint betID) {
        Event storage ev = events[eventID];
        require(ev.availableBets[ev.idToIndex[betID]].odd > 0);
        _;
    }
    
    modifier betOpen(uint eventID, uint betID) {
        Event storage ev = events[eventID];
        Bet storage bet = ev.availableBets[ev.idToIndex[betID]];
        require(!ev.isBetTypeClosed[bet.betType]);
        _;
    }
    
    modifier placedBetExists(uint eventID, uint betIndex) {
        Event storage ev = events[eventID];
        require(ev.placedBets[betIndex].odd > 0);
        _;
    }
    
    modifier eventActive(uint eventID) {
        require(events[eventID].ts > now);
        _;
    }
    
    //uint[] tmpTest;
    //uint[] tmpTest2;
    
    constructor() public {
        owner = msg.sender;
        
        //DEBUG temp adds:
        //tmpTest.length = 2;
        //tmpTest[0] = 1;
        //tmpTest[1] = 2;
        
        //DEBUG temp adds:
        //tmpTest2.length = 2;
        //tmpTest2[0] = 1;
        //tmpTest2[1] = 2;
        
        //createEvent(1, 1629829173, "Bulg", "Germ", "Football", tmpTest);

        //updateBet(1, 1, 123123, [uint(1),3], 7043, 1);
        //updateBet(1, 2, 321321, [uint(2),1], 1034, 2);
        
        //createEvent(2, 1629829173, "Isla", "Germ", "Football", tmpTest2);

        //updateBet(2, 1, 420, [uint(5),2], 2043, 1);
        //updateBet(2, 2, 453, [uint(1),3], 1014, 2);
        
        //createEvent(3, 1629829173, "Germ", "Fran", "Football", tmpTest);

        //updateBet(2, 1, 3562, [uint(0),0], 2043, 1);
        //updateBet(2, 2, 23748, [uint(1),1], 5034, 2);
        
        //_placeBetObject(0x123, 100, 1, 321321);
        //_placeBetObject(0x123, 90, 2, 453);
        //_placeBetObject(0x123, 400, 3, 3562);
    }
    
    function getEventIDs() public view returns (uint[]) {
        return eventIDs;
    }
    
    function getEventByID(uint eventID) public eventExists(id) view returns (uint id, uint ts, string home, string away, string sport, uint[] betTypes, uint[] closedBetTypes, bool active) {
        Event storage ev = events[eventID];
        id = eventID;
        ts = ev.ts;
        home = ev.home;
        away = ev.away;
        sport = ev.sport;
        betTypes = ev.betTypes;
        active = ev.active;
        closedBetTypes = ev.closedBetTypes;
    }
    
    function breakDownBet(Bet bet) private view returns (uint id, uint amount, uint betType, uint[2] values, uint odd, address user, uint number) {
        id = bet.id;
        amount = bet.amount;
        betType = bet.betType;
        values = bet.values;
        odd = bet.odd;
        user = bet.user;
        number = bet.number;
    }
    
    function getEventBetByID(uint eventID, uint betID) public betExists(eventID, betID) view returns (uint id, uint amount, uint betType, uint[2] values, uint odd, address user, uint number) {
        Event storage ev = events[eventID];
        Bet storage bet = ev.availableBets[ev.idToIndex[betID]];
        
        id = bet.id;
        amount = bet.amount;
        betType = bet.betType;
        values = bet.values;
        odd = bet.odd;
        user = bet.user;
        number = bet.number;
        //return breakDownBet(bet);
    }
    
    function getEventPlacedBetByIndex(uint eventID, uint idx) public placedBetExists(eventID, idx) view returns (uint id, uint amount, uint betType, uint[2] values, uint odd, address user, uint number) {
        Event storage ev = events[eventID];
        Bet storage bet = ev.placedBets[idx];
        
        id = bet.id;
        amount = bet.amount;
        betType = bet.betType;
        values = bet.values;
        odd = bet.odd;
        user = bet.user;
        number = bet.number;
        //return breakDownBet(bet);
    }
    
    function getEventAvailableBets(uint eventID) public eventExists(eventID) view returns (uint[]) {
        return events[eventID].availableBetsIds;
    }
    
    function getEventPlacedBetsAmount(uint eventID) public eventExists(eventID) view returns (uint) {
        return events[eventID].placedBets.length;
    }
    
    function isCallerAdmin() public view returns(bool) {
        return msg.sender == owner;
    }
    
    function createEvent(uint id, uint ts, string home, string away, string sport, uint[] betTypes) onlyOwner public {
        Event storage ev = events[id];
        ev.id = id;
        ev.ts = ts;
        ev.away = away;
        ev.home = home;
        ev.sport = sport;
        ev.betTypes = betTypes;
        ev.active = true;
        
        eventIDs.push(id);
    }
    
    function updateBet(uint eventID, uint betType, uint betID, uint[2] values, uint odd, uint number) public eventActive(eventID) onlyOwner {
        Event storage ev = events[eventID];
        
        uint idx = 0;
        
        if(ev.idToIndex[betID] == 0){
            ev.availableBets.length++;
            idx = ev.availableBets.length-1;
            ev.availableBetsIds.push(betID);
            ev.idToIndex[betID] = idx;
        } else {
            idx = ev.idToIndex[betID];
        }
        
        Bet storage bet = ev.availableBets[idx];

        bet.eventID = eventID;
        bet.id = betID;
        bet.betType = betType;
        bet.values = values;
        bet.odd = odd;
        bet.number = number;
    }
    
    function _placeBetObject(address sender, uint amount, uint eventID, uint betID) private {
        Event storage ev = events[eventID];
        Bet storage avBet = ev.availableBets[ev.idToIndex[betID]];
        Bet memory b = avBet; // copy to memory
        b.amount = amount;
        b.user = sender;
        
        ev.placedBets.push(b);
    }
    
    //only if event is active
    //if bet is not closed
    function placeBet(uint eventID, uint betID) public eventActive(eventID) betExists(eventID, betID) betOpen(eventID, betID) payable {
        require(msg.value > 0);
        
        _placeBetObject(msg.sender, msg.value, eventID, betID);
    }
    
    function announceResult(uint eventID, uint betType, uint[2] values) public onlyOwner {
        Event storage ev = events[eventID];
        
        for(uint i=0; i<ev.placedBets.length; i++) {
            Bet storage bet = ev.placedBets[i];
            
            if(bet.betType == betType && bet.values[0] == values[0] && bet.values[1] == values[1]) {
                uint winnings = bet.amount * bet.odd;
                winnings /= 100; //because odd is multiplied by 100;
                withdrawals[bet.user] += winnings;
                
                emit BetWon(bet.user, eventID, i, winnings);
            }
        }
    }
    
    function withdraw(uint value) public {
        require(value <= withdrawals[msg.sender]);
        
        withdrawals[msg.sender] -= value;
        msg.sender.transfer(value);
    }
    
    function closeBetType(uint eventID, uint betType) public eventExists(eventID) onlyOwner {
        Event storage ev = events[eventID];
        
        if(!ev.isBetTypeClosed[betType]) {
            ev.isBetTypeClosed[betType] = true;
            ev.closedBetTypes.push(betType);
        }
        
        for(uint i=0; i < ev.betTypes.length; i++) {
            //TODO
        }
    }
}