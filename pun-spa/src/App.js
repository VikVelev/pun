import React, { Component } from 'react';
import './App.css';
import { Route, Switch, Redirect, BrowserRouter as Router } from 'react-router-dom';
import BetPage from './component/main'
import AdminPage from './component/admin'
import { testNN } from './nn'

class App extends Component {

	componentDidMount() {
		testNN(); //eslint-disable-line
	}

	routes = {
		main: () => <BetPage/>,
		admin: () => <AdminPage/>
	}	

	render() {
		return (
			<Router className="root">
				<Switch>
					<Route exact path="/" component={this.routes.main}/>
					<Route exact path="/admin" component={this.routes.admin}/>					
				</Switch>
			</Router>
		);
	}
}

export default App;
