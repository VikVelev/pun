import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import 'semantic-ui-css/semantic.min.css';


//dont ask
setTimeout(() => ReactDOM.render(<App />, document.getElementById('root')), 1);
registerServiceWorker();
